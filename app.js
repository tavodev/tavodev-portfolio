const btnHamburger = document.querySelector("#btnHamburger");
const header = document.querySelector(".header");
const menu = document.querySelector(".menu");
const body = document.querySelector("body");

btnHamburger.addEventListener("click", function(){
  console.log('click hamburger');

  if(header.classList.contains("open")){ // Close Hamburger Menu
    body.classList.remove("noscroll"); 
    header.classList.remove("open");  
    menu.classList.remove("show");  
 }

  else { // Open Hamburger Menu
    body.classList.add("noscroll"); 
    header.classList.add("open");
    menu.classList.add("show"); 
}  
});

document.addEventListener('click', (e) => {
  if(e.target.matches('.menu a')){
    body.classList.remove('noscroll');
    header.classList.remove('open');
    menu.classList.remove('show');
    buttonTop.classList.remove('shows');
  }
}
);


/*-------------BUTTON UP---------------*/

const buttonTop = document.querySelector('#buttonTop');

window.onscroll = () => {
    if (document.documentElement.scrollTop > 100) {
        buttonTop.classList.add('shows');
    } else {
        buttonTop.classList.remove('shows');
    }
    buttonTop.addEventListener('click', () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        })
    })
}


/* ********** ContactForm ********** */
((d) => {
  const $form = d.querySelector(".contact-form"),
    $loader = d.querySelector(".contact-form-loader"),
    $response = d.querySelector(".contact-form-response");

  $form.addEventListener("submit", (e) => {
    e.preventDefault();
    $loader.classList.remove("none");
    fetch("https://formsubmit.co/ajax/tavoespinella@gmail.com", {
      method: "POST",
      body: new FormData(e.target),
    })
      .then((res) => (res.ok ? res.json() : Promise.reject(res)))
      .then((json) => {
        console.log(json);
        location.hash = "#gracias";
        $form.reset();
      })
      .catch((err) => {
        console.log(err);
        let message =
          err.statusText || "Ocurrió un error al enviar, intenta nuevamente";
        $response.querySelector(
          "h3"
        ).innerHTML = `Error ${err.status}: ${message}`;
      })
      .finally(() => {
        $loader.classList.add("none");
        setTimeout(() => {
          location.hash = "#close";
        }, 3000);
      });
  });
})(document);